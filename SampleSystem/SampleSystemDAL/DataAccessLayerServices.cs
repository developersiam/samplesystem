﻿namespace SampleSystemDAL
{
    public static class DataAccessLayerServices
    {
        public static IQC_Sample_RoleRepository QC_Sample_RoleRepository()
        {
            IQC_Sample_RoleRepository obj = new QC_Sample_RoleRepository(new StecDBMSEntities1());
            return obj;
        }
        public static IQC_Sample_UserRoleRepository QC_Sample_UserRoleRepository()
        {
            IQC_Sample_UserRoleRepository obj = new QC_Sample_UserRoleRepository(new StecDBMSEntities1());
            return obj;
        }
        public static IQC_Sample_UserAccountRepository QC_Sample_UserAccountRepository()
        {
            IQC_Sample_UserAccountRepository obj = new QC_Sample_UserAccountRepository(new StecDBMSEntities1());
            return obj;
        }
        public static IPackedGradeRepository PackedGradeRepository()
        {
            IPackedGradeRepository obj = new PackedgradeRepository(new StecDBMSEntities1());
            return obj;
        }
        public static IPDSetupRepository PDSetupRepository()
        {
            IPDSetupRepository obj = new PDSetupRepository(new StecDBMSEntities1());
            return obj;
        }
        public static ITypeRepository TypeRepository()
        {
            ITypeRepository obj = new TypeRepository(new StecDBMSEntities1());
            return obj;
        }
        public static IQC_SampleRepository QC_SampleRepository()
        {
            IQC_SampleRepository obj = new QC_SampleRepository(new StecDBMSEntities1());
            return obj;
        }
        public static IQC_Sample_ShippingRepository QC_Sample_ShippingRepository()
        {
            IQC_Sample_ShippingRepository obj = new QC_Sample_ShippingRepository(new StecDBMSEntities1());
            return obj;
        }
        public static IQC_Sample_ShipDetailsRepository QC_Sample_ShipDetailsRepository()
        {
            IQC_Sample_ShipDetailsRepository obj = new QC_Sample_ShipDetailsRepository(new StecDBMSEntities1());
            return obj;
        }
        public static IQC_Sample_CustomerRepository QC_Sample_CustomerRepository()
        {
            IQC_Sample_CustomerRepository obj = new QC_Sample_CustomerRepository(new StecDBMSEntities1());
            return obj;
        }
        public static IQC_Sample_AddrRepository QC_Sample_AddrRepository()
        {
            IQC_Sample_AddrRepository obj = new QC_Sample_AddrRepository(new StecDBMSEntities1());
            return obj;
        }
    }
}
