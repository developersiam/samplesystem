﻿using System.Data.Entity;
using DomainModel;

namespace SampleSystemDAL
{
    public interface IPackedGradeRepository : IGenericDataRepository<packedgrade> { }
    public class PackedgradeRepository : GenericDataRepository<packedgrade>, IPackedGradeRepository { public PackedgradeRepository(DbContext db) : base(db) { } }
    public interface IPDSetupRepository : IGenericDataRepository<pdsetup> { }
    public class PDSetupRepository : GenericDataRepository<pdsetup>, IPDSetupRepository { public PDSetupRepository(DbContext db) : base(db) { } }
    public interface IQC_Sample_RoleRepository : IGenericDataRepository<QC_sample_Role> { }
    public class QC_Sample_RoleRepository : GenericDataRepository<QC_sample_Role>, IQC_Sample_RoleRepository { public QC_Sample_RoleRepository(DbContext db) : base(db) { } }
    public interface IQC_Sample_UserRoleRepository : IGenericDataRepository<QC_Sample_UserRole> { }
    public class QC_Sample_UserRoleRepository : GenericDataRepository<QC_Sample_UserRole>, IQC_Sample_UserRoleRepository { public QC_Sample_UserRoleRepository(DbContext db) : base(db) { } }
    public interface IQC_Sample_UserAccountRepository : IGenericDataRepository<QC_Sample_UserAccount> { }
    public class QC_Sample_UserAccountRepository : GenericDataRepository<QC_Sample_UserAccount>, IQC_Sample_UserAccountRepository { public QC_Sample_UserAccountRepository(DbContext db) : base(db) { } }
    public interface ITypeRepository : IGenericDataRepository<type> { }
    public class TypeRepository : GenericDataRepository<type>, ITypeRepository { public TypeRepository(DbContext db) : base(db) { } }
    public interface IQC_SampleRepository : IGenericDataRepository<QC_Sample> { }
    public class QC_SampleRepository : GenericDataRepository<QC_Sample>, IQC_SampleRepository { public QC_SampleRepository(DbContext db) : base(db) { } }
    public interface IQC_Sample_ShippingRepository : IGenericDataRepository<QC_Sample_Shipping> { }
    public class QC_Sample_ShippingRepository : GenericDataRepository<QC_Sample_Shipping>, IQC_Sample_ShippingRepository { public QC_Sample_ShippingRepository(DbContext db) : base(db) { } }
    public interface IQC_Sample_ShipDetailsRepository : IGenericDataRepository<QC_Sample_Shipping_Details> { }
    public class QC_Sample_ShipDetailsRepository : GenericDataRepository<QC_Sample_Shipping_Details>, IQC_Sample_ShipDetailsRepository { public QC_Sample_ShipDetailsRepository(DbContext db) : base(db) { } }
    public interface IQC_Sample_CustomerRepository : IGenericDataRepository<QC_Sample_Customer> { }
    public class QC_Sample_CustomerRepository : GenericDataRepository<QC_Sample_Customer>, IQC_Sample_CustomerRepository { public QC_Sample_CustomerRepository(DbContext db) : base(db) { } }
    public interface IQC_Sample_AddrRepository : IGenericDataRepository<QC_Sample_Address> { }
    public class QC_Sample_AddrRepository : GenericDataRepository<QC_Sample_Address>, IQC_Sample_AddrRepository { public QC_Sample_AddrRepository(DbContext db) : base(db) { } }
}
