﻿using SampleSystem.Forms;
using SampleSystem.Forms.Customer;
using SampleSystem.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SampleSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void HomeMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SampleMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DuplicateMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (user_setting.User == null)
                //{
                //    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                //    MainFrame.NavigationService.Navigate(new Forms.Login());
                //    return;
                //}

                //if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0)
                //    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.NavigationService.Navigate(new Forms.Stock.SampleStockList());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ShipmentMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (user_setting.User == null)
                //{
                //    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                //    MainFrame.NavigationService.Navigate(new Forms.Login());
                //    return;
                //}

                //if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0)
                //    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.NavigationService.Navigate(new Forms.Shipping.SampleShippingList());


            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DocumentMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void InvoiceMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void AOITagMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void LogOffMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                user_setting.User = null;
                user_setting.UserRoles = null;
                MainFrame.NavigationService.Navigate(new Forms.Login());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageUserMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Forms.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Forms.UserAccounts.UserAccounts());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        private void CustomerMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (user_setting.User == null)
                //{
                //    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                //    MainFrame.NavigationService.Navigate(new Forms.Login());
                //    return;
                //}

                //if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0)
                //    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                CustomerWindow window = new CustomerWindow();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
