﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleSystem.ViewModel
{
    public class qc_sample_details_vm : QC_Sample_Shipping_Details
    {
        public string Type { get; set; }
        public decimal TotalPrices { get; set; }
    }
}
