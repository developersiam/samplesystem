﻿using DomainModel;


namespace SampleSystem.ViewModel
{
    public class qc_sample_customer_vm : QC_Sample_Address
    {
        public string Name { get; set; }
        public string Attn { get; set; }
    }
}
