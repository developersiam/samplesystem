﻿using DomainModel;
using SampleSystem.Forms.Customer;
using SampleSystem.Helper;
using SampleSystem.ViewModel;
using SampleSystemBL;
using System;
using System.Linq;
using System.Windows;

using System.Windows.Input;

namespace SampleSystem.Forms.Shipping
{
    /// <summary>
    /// Interaction logic for SampleShipping.xaml
    /// </summary>
    public partial class SampleShipping : Window
    {
        int Curr_ShippingId;
        string Curr_InvNo;
        int Curr_SampleId;
        public SampleShipping(string invno)
        {
            InitializeComponent();
            Curr_InvNo = invno;
            ToDatePicker.SelectedDate = DateTime.Now;

            CustCombobox.ItemsSource = null;
            CustCombobox.ItemsSource = BusinessLayerServices.QC_sample_CustomerBL().GetAll();
            //CustCombobox.ItemsSource = Helper.QC_Sample_Customer_Helper.GetAllCustomer().ToList();


            //------------ Edit details ------------
            if (!String.IsNullOrEmpty(Curr_InvNo)) BindingData();
        }

        private void BindingData()
        {
            try
            {
                invTextBox.Text = Curr_InvNo;

                var existed = BusinessLayerServices.QC_sample_ShippingBL().GetSingle(Curr_InvNo);
                if (existed != null)
                {
                    Curr_ShippingId = existed.ShippingId;
                    CustCombobox.SelectedValue = existed.QC_Sample_Customer.CustomerId;

                    AddrCombobox.ItemsSource = null;
                    AddrCombobox.ItemsSource = BusinessLayerServices.QC_sample_AddrBL().GetAllAddr(Convert.ToInt16(CustCombobox.SelectedValue));
                    AddrCombobox.SelectedValue = existed.AddressId;

                    grossTextBox.Text = existed.Gross_wt.ToString();
                    tareTextBox.Text = existed.Tare_wt.ToString();
                    destinationTextBox.Text = existed.Destination;
                    ToDatePicker.SelectedDate = existed.DateTo;
                    attnTextBox.Text = existed.Attn;
                    incoTextBox.Text = existed.IncoTerm;
                    ABWTextBox.Text = existed.ABW;
                    courierTextBox.Text = existed.Courier;
                    portTextBox.Text = existed.Port;
                    NoteTextBox.Text = existed.Note;

                    ////Blinding to item stock
                    //BlindingStock();
                    ////Disable all tools
                    //DisableAll();
                    //Blinding to datagrid

                    EnableObject();
                    GetSampleShippingDetails();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SCropTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //Binding sample from criteria
            ReloadDataGrid();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BFDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (qc_sample_details_vm)BFDataGrid.SelectedItem;
                BusinessLayerServices.QC_sample_ShippingDBL().DeleteSampleItem(model.Shipping_DetailsId);

                GetSampleShippingDetails();
                ReloadDataGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            //Add to Sample Shipping
            try
            {
                int Scrop = DateTime.Now.Year;
                if (!string.IsNullOrEmpty(invTextBox.Text))
                {
                    var SampleShip = new QC_Sample_Shipping
                    {
                        Crop = Scrop,
                        InvoiceNo = invTextBox.Text,
                        Customer = CustCombobox.Text,
                        CustomerId = Convert.ToInt16(CustCombobox.SelectedValue),
                        AddressId = Convert.ToInt16(AddrCombobox.SelectedValue),
                        Gross_wt = grossTextBox.Text != "" ? Convert.ToDecimal(grossTextBox.Text) : 0,
                        Tare_wt = tareTextBox.Text != "" ? Convert.ToDecimal(tareTextBox.Text) : 0,
                        Destination = destinationTextBox.Text,
                        Attn = attnTextBox.Text,
                        IncoTerm = incoTextBox.Text,
                        ABW = ABWTextBox.Text,
                        Courier = courierTextBox.Text,
                        Port = portTextBox.Text,
                        Note = NoteTextBox.Text,
                        //User = user_setting.User.Username,
                        DateTo = ToDatePicker.SelectedDate,
                    };

                    //Check Duplicated
                    QC_Sample_Shipping existed = BusinessLayerServices.QC_sample_ShippingBL().GetSingle(invTextBox.Text);
                    if (existed != null)
                    {
                        //Binding new one to existed one
                        Curr_ShippingId = existed.ShippingId;
                        BusinessLayerServices.QC_sample_ShippingBL().Edit(SampleShip);
                    }
                    else Curr_ShippingId = BusinessLayerServices.QC_sample_ShippingBL().Add(SampleShip);

                    EnableObject();

                }
                else MessageBoxHelper.Warning("กรุณากรอก Invoice No.");

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EnableObject()
        {
            AddSampleButton.IsEnabled = true;
            TypeCombobox.ItemsSource = null;
            TypeCombobox.ItemsSource = BusinessLayerServices.TypeBL().GetAllTypes();
        }

        private void AddSampleButton_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(SCropTextBox.Text)) throw new ArgumentException("Please fill Stec Crop");
            if (String.IsNullOrEmpty(cCropTextBox.Text)) throw new ArgumentException("Please fill Customer Crop");
            if (String.IsNullOrEmpty(sGradeTextBox.Text)) throw new ArgumentException("Please fill Stec Grade");
            if (String.IsNullOrEmpty(cGradeTextBox.Text)) throw new ArgumentException("Please fill Customer Grade");
            if (String.IsNullOrEmpty(sCaseNoTextBox.Text)) throw new ArgumentException("Please fill Stec Case No.");
            //if (String.IsNullOrEmpty(cCaseNoTextBox.Text)) throw new ArgumentException("Please fill Customer Case No.");

            if (!string.IsNullOrEmpty(unitPriceTextBox.Text))
            {
                if (Helper.RegularExpressionHelper.IsDecimalCharacter(unitPriceTextBox.Text) == false) throw new ArgumentException("Unit Price จะต้องเป็นตัวเลขเท่านั้น");
            }
            if (!string.IsNullOrEmpty(QtyTextBox.Text))
            {
                if (Helper.RegularExpressionHelper.IsNumericCharacter(QtyTextBox.Text) == false) throw new ArgumentException("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");
            }
            if (!string.IsNullOrEmpty(WeightTextBox.Text))
            {
                if (Helper.RegularExpressionHelper.IsDecimalCharacter(WeightTextBox.Text) == false) throw new ArgumentException("Weight จะต้องเป็นตัวเลขเท่านั้น");
            }

            AddSampleShippingDetails();
            GetSampleShippingDetails();
            ReloadDataGrid();

            ClearContent();
        }

        private void GetSampleShippingDetails()
        {
            if (string.IsNullOrEmpty(invTextBox.Text)) return;
            //Show shipping Detail in list
            var lst = Helper.QC_Sample_ShippingD_Helper.GetShippingDetails(invTextBox.Text).ToList();

            if (lst == null) return;
            BFDataGrid.ItemsSource = null;
            BFDataGrid.ItemsSource = lst;

            TotalTextBlock2.Text = lst.Count().ToString();
        }
        private void ClearContent()
        {
            //Clear Textbox
            cCropTextBox.Text = "";
            sGradeTextBox.Text = "";
            cGradeTextBox.Text = "";
            oGradeTextBox.Text = "";
            FormTextBox.Text = "";
            SizeTextBox.Text = "";
            WeightTextBox.Text = "";
            sCaseNoTextBox.Text = "";
            cCaseNoTextBox.Text = "";
            QtyTextBox.Text = "";
            unitPriceTextBox.Text = "";
            NicTextBox.Text = "";
            SugTextBox.Text = "";
            SampleInvTextBox.Text = "";
        }

        private void AddSampleShippingDetails()
        {
            BusinessLayerServices.QC_sample_ShippingDBL().Add(new QC_Sample_Shipping_Details
            {
                ShippingId = Curr_ShippingId,
                SampleId = Curr_SampleId,
                SampleType = SampleTypeCombobox.Text,
                Crop = Convert.ToInt16(SCropTextBox.Text),
                CustomerCrop = Convert.ToInt16(cCropTextBox.Text),
                StecPackedGrade = sGradeTextBox.Text,
                CustomerGrade = cGradeTextBox.Text,
                OfferGrade = oGradeTextBox.Text,
                Form = FormTextBox.Text,
                StecCaseNo = sCaseNoTextBox.Text,
                CustomerCaseNo = cCaseNoTextBox.Text,
                Quantity = string.IsNullOrEmpty(QtyTextBox.Text) ? 0 : Convert.ToInt32(QtyTextBox.Text),
                Weight = string.IsNullOrEmpty(WeightTextBox.Text) ? 0 : Convert.ToDecimal(WeightTextBox.Text),
                UnitPrices = string.IsNullOrEmpty(unitPriceTextBox.Text) ? 0 : Convert.ToDecimal(unitPriceTextBox.Text),
                ModifiedDate = DateTime.Now,
                //User = user_setting.User.Username
            });
        
        }

        private void SampleStockDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (SampleStockDataGrid.SelectedIndex < 0) return;

                var selected = (qc_sample_vm)SampleStockDataGrid.SelectedItem;
                if (selected != null)
                {
                    Curr_SampleId = selected.SampleId;
                    SCropTextBox.Text = selected.Crop.ToString();
                    TypeCombobox.Text = selected.Type;
                    sGradeTextBox.Text = selected.StecPackedGrade;
                    SampleTypeCombobox.Text = selected.SampleType;
                    FormTextBox.Text = selected.TobaccoType;
                    SizeTextBox.Text = selected.CartonDimensions;
                    sCaseNoTextBox.Text = selected.StecCaseNo;
                    QtyTextBox.Text = selected.Quantity.ToString();
                    WeightTextBox.Text = selected.Weight.ToString();
                    NoteTextBox.Text = selected.Note;
                    SampleInvTextBox.Text = selected.InvoiceNo;
                    NicTextBox.Text = selected.Nicotine.ToString();
                    SugTextBox.Text = selected.Sug.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SampleTypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
             ReloadDataGrid();
        }

        private void ReloadDataGrid()
        {
            if (string.IsNullOrEmpty(TypeCombobox.Text) || string.IsNullOrEmpty(SCropTextBox.Text)) return;


            var lst = Helper.QC_Sample_Helper.GetSampleByTypeCrop(TypeCombobox.Text, Convert.ToInt16(SCropTextBox.Text)).ToList();
            if (lst == null) return;

            SampleStockDataGrid.ItemsSource = null;
            SampleStockDataGrid.ItemsSource = lst;
            TotalTextBlock.Text = lst.Count().ToString();
        }

        private void CustCombobox_DropDownClosed(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(CustCombobox.Text)) return;
            //fill attn text box
            attnTextBox.Text = BusinessLayerServices.QC_sample_CustomerBL().GetSingle(CustCombobox.Text).Attn.ToString();

            //fill Addr from Address Table
            AddrCombobox.ItemsSource = null;
            AddrCombobox.ItemsSource = BusinessLayerServices.QC_sample_AddrBL().GetAllAddr(Convert.ToInt16(CustCombobox.SelectedValue));
        }

        private void CustButton_Click(object sender, RoutedEventArgs e)
        {
            CustomerWindow window = new CustomerWindow();
            window.ShowDialog();
        }
    }
}
