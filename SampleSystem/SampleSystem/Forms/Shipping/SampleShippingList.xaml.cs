﻿using DomainModel;
using SampleSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SampleSystem.Forms.Shipping
{
    /// <summary>
    /// Interaction logic for SampleShippingList.xaml
    /// </summary>
    public partial class SampleShippingList : Page
    {
        public SampleShippingList()
        {
            InitializeComponent();
            BindingInvCrop();
        }

        private void BindingInvCrop()
        {
            List<int> cropLst = new List<int>();
            cropLst = BusinessLayerServices.QC_sample_ShippingBL().GetInvoiceCrop();
            CropCombobox.ItemsSource = null;
            CropCombobox.ItemsSource = cropLst;

            CropCombobox.Text = cropLst.FirstOrDefault().ToString();
        }

        private void CropCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Binding Data to Grid
            ReloadGrid();
        }

        private void ReloadGrid()
        {
            if (CropCombobox.SelectedValue.ToString() == "") return;
            ShippingDataGrid.ItemsSource = null;
            ShippingDataGrid.ItemsSource = BusinessLayerServices.QC_sample_ShippingBL().GetShippingDataByCrop(Convert.ToInt16(CropCombobox.SelectedValue.ToString()));
        }

        private void ShippingDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (ShippingDataGrid.SelectedIndex < 0)
                    return;

                var model = (QC_Sample_Shipping)ShippingDataGrid.SelectedItem;

                SampleShipping window = new SampleShipping(model.InvoiceNo);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            SampleShipping ss = new SampleShipping("");
            ss.ShowDialog();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (ShippingDataGrid.SelectedIndex < 0) return;
            var msg = MessageBox.Show("ต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (msg == MessageBoxResult.No)
                return;

            var selected = (QC_Sample_Shipping)ShippingDataGrid.SelectedItem;

            //Delete in Shipping Details
            BusinessLayerServices.QC_sample_ShippingDBL().DeletebyInvno(selected.ShippingId);

            //Delete in Shipping Header
            BusinessLayerServices.QC_sample_ShippingBL().Delete(selected.InvoiceNo);

            ReloadGrid();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadGrid();
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            if (ShippingDataGrid.SelectedIndex < 0) return;

            var selected = (QC_Sample_Shipping)ShippingDataGrid.SelectedItem;

            Invoice.Invoice1 w = new Invoice.Invoice1();
            //Reports.RPT_RC007 window = new Reports.RPT_RC007(DocumentNo.Text);
            w.ShowDialog();

        }
    }
}
