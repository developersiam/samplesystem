﻿using DomainModel;
using MaterialDesignThemes.Wpf;
using SampleSystem.Helper;
using SampleSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;


namespace SampleSystem.Forms.UserAccounts
{
    public struct UserRoleDataGrid
    {
        public Guid RoleID { get; set; }
        public string RoleName { get; set; }
        public bool AssignStatus { get; set; }
    }
    public partial class UserRoles : Window
    {
        QC_Sample_UserAccount _user;
        public UserRoles(QC_Sample_UserAccount user)
        {
            InitializeComponent();

            _user = new QC_Sample_UserAccount();
            _user = user;

            BindingUserRoleDataGrid();
        }

        private void BindingUserRoleDataGrid()
        {
            try
            {
                var list = from r in BusinessLayerServices.QC_sample_RoleBL().GetAll()
                           join u in BusinessLayerServices.QC_sample_UserRoleBL().GetByUsername(_user.Username)
                           on r.RoleID equals u.RoleID into ur
                           from rs in ur.DefaultIfEmpty()
                           select new UserRoleDataGrid
                           {
                               RoleID = r.RoleID,
                               RoleName = r.RoleName,
                               AssignStatus = rs == null ? false : true
                           };

                UserRoleDataGrid.ItemsSource = null;
                UserRoleDataGrid.ItemsSource = list.OrderBy(x => x.RoleName);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        private void UserRoleDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (UserRoleDataGrid.SelectedIndex < 0)
                    return;

                var model = (UserRoleDataGrid)UserRoleDataGrid.SelectedItem;
                if (!model.AssignStatus)
                    BusinessLayerServices.QC_sample_UserRoleBL()
                        .Add(new QC_Sample_UserRole
                        {
                            Username = _user.Username,
                            RoleID = model.RoleID,
                            CreateDate = DateTime.Now,
                            CreateBy = user_setting.User.Username
                        });
                else
                    BusinessLayerServices.QC_sample_UserRoleBL().Delete(_user.Username, model.RoleID);

                BindingUserRoleDataGrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void RoleButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Roles window = new Roles();
                window.ShowDialog();

                BindingUserRoleDataGrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
