﻿using SampleSystemBL;
using System;
using System.Windows;
using System.Windows.Controls;
using SampleSystem.Helper;
using DomainModel;

namespace SampleSystem.Forms.UserAccounts
{
    /// <summary>
    /// Interaction logic for UserAccounts.xaml
    /// </summary>
    public partial class UserAccounts : Page
    {
        public UserAccounts()
        {
            InitializeComponent();
            DataGridBinding();
        }

        private void DataGridBinding()
        {
            try
            {
                UserAccountDataGrid.ItemsSource = null;
                UserAccountDataGrid.ItemsSource = BusinessLayerServices.QC_sample_UserAccountBL().GetAll();

                TotalTextBlock.Text = UserAccountDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        private void Clear()
        {
            UsernameTextBox.Clear();
            AddButton.IsEnabled = false;
        }
        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก username");

                if (Helper.UserAccountHelper.ValidateUserNameInActiveDirectory(UsernameTextBox.Text) == false)
                    throw new ArgumentException("ไม่พบข้อมูลผู้ใช้ " + UsernameTextBox.Text + " ในระบบ");

                AddButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UserAccountDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?");
                if (msg == MessageBoxResult.No)
                    return;

                var model = (QC_Sample_UserAccount)UserAccountDataGrid.SelectedItem;
                BusinessLayerServices.QC_sample_UserAccountBL().Delete(model.Username);

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddRoleButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UserAccountDataGrid.SelectedIndex < 0)
                    return;

                var model = (QC_Sample_UserAccount)UserAccountDataGrid.SelectedItem;
                UserRoles window = new UserRoles(model);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก username");

                if (Helper.UserAccountHelper.ValidateUserNameInActiveDirectory(UsernameTextBox.Text) == false)
                    throw new ArgumentException("ไม่พบข้อมูลผู้ใช้ " + UsernameTextBox.Text + " ในระบบ");

                BusinessLayerServices.QC_sample_UserAccountBL().Add(new DomainModel.QC_Sample_UserAccount
                {
                    Username = UsernameTextBox.Text,
                    CreateBy = "aphiradee",
                    CreateDate = DateTime.Now
                });

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
