﻿using DomainModel;
using SampleSystem.Helper;
using SampleSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SampleSystem.Forms.Stock
{
    /// <summary>
    /// Interaction logic for SampleStockList.xaml
    /// </summary>
    public partial class SampleStockList : Page
    {
        public SampleStockList()
        {
            InitializeComponent();
            BindingData();

        }
        private void BindingData()
        {
            TypeCombobox.ItemsSource = null;
            TypeCombobox.ItemsSource = BusinessLayerServices.TypeBL().GetAllTypes();

            var cLst = BusinessLayerServices.QC_sampleBL().GetCrop(); //.Select(x => x.CuttingDate.Year);
            CropCombobox.ItemsSource = null;
            CropCombobox.ItemsSource = cLst;

            CropCombobox.SelectedValue = cLst;
        }

        private void ReloadDatagrid()
        {
            try
            {
                if (string.IsNullOrEmpty(CropCombobox.Text) || string.IsNullOrEmpty(TypeCombobox.Text)) return;
                var smpLst = BusinessLayerServices.QC_sampleBL().GetByCropType(Convert.ToInt16(CropCombobox.Text), TypeCombobox.Text);
                SampleStockDataGrid.ItemsSource = null;
                SampleStockDataGrid.ItemsSource = smpLst;

                TotalTextBlock.Text = smpLst.Count().ToString();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            SampleStock ss = new SampleStock(0);
            ss.ShowDialog();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (SampleStockDataGrid.SelectedIndex < 0) return;
            var msg = MessageBox.Show("ต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (msg == MessageBoxResult.No)
                return;

            var selected = (QC_Sample)SampleStockDataGrid.SelectedItem;

            //Check sample in Shipping BF delete
            string InvNo = BusinessLayerServices.QC_sample_ShippingDBL().GetInvoiceNo(selected.SampleId);
            if (!string.IsNullOrEmpty(InvNo))
            {
                MessageBoxHelper.Danger("Sample นี้ อยุ่ใน Invoice No." + InvNo + " จึงไม่สามารถลบได้");
                return;
            }
              
            BusinessLayerServices.QC_sampleBL().Delete(selected.SampleId);
            ReloadDatagrid();
        }

        private void SampleStockDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (SampleStockDataGrid.SelectedIndex < 0) return;

                var selected = (QC_Sample)SampleStockDataGrid.SelectedItem;
                SampleStock ss = new SampleStock(selected.SampleId);
                ss.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void TypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid();
        }

        private void CropCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }
    }
}
