﻿using DomainModel;
using SampleSystem.Helper;
using SampleSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SampleSystem.Forms
{
    /// <summary>
    /// Interaction logic for SampleStock.xaml
    /// </summary>
    public partial class SampleStock : Window
    {
        int Curr_sample;
        public SampleStock(int sampleId)
        {
            InitializeComponent();
            BindingData();

            Curr_sample = sampleId;
            if (sampleId != 0) ReloadSample();
        }

        private void BindingData()
        {
            TypeCombobox.ItemsSource = null;
            TypeCombobox.ItemsSource = BusinessLayerServices.TypeBL().GetAllTypes();

            CropTextBox.Text = DateTime.Now.Year.ToString();
            CuttingDatePicker.SelectedDate = DateTime.Now;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int Scrop = Convert.ToInt16(CropTextBox.Text);
                if (VerifyBFAdd())
                {
                    var SampleStock = new QC_Sample
                    {
                        Crop = Scrop,
                        Type = TypeCombobox.SelectedItem.ToString(),
                        StecPackedGrade = stecGradeCombobox.Text,
                        SampleType = SampleTypeCombobox.Text,
                        StecCaseNo = CaseNoTextBox.Text,
                        TobaccoType = FormTextBox.Text,
                        CartonDimensions = SizeTextBox.Text,
                        Quantity = Convert.ToInt16(QtyTextBox.Text),
                        Weight = Convert.ToDecimal(WeightTextBox.Text),
                        Location = LocationTextBox.Text,
                        CuttingDate = (DateTime)CuttingDatePicker.SelectedDate,
                        Note = NoteTextBox.Text,
                        Nicotine = string.IsNullOrEmpty(NicTextBox.Text) ? 0 : Convert.ToDecimal(NicTextBox.Text),
                        Sug = string.IsNullOrEmpty(SugTextBox.Text) ? 0 : Convert.ToDecimal(SugTextBox.Text),
                        ModifiedDate = DateTime.Now,
                    };

                    //Check Duplicated
                    QC_Sample Sample_id = BusinessLayerServices.QC_sampleBL().ExistedSample(Scrop, TypeCombobox.Text, stecGradeCombobox.Text, (DateTime)CuttingDatePicker.SelectedDate, CaseNoTextBox.Text);
                    if (Sample_id != null)
                    {
                        //Binding new one to existed one
                        BusinessLayerServices.QC_sampleBL().Edit(SampleStock);
                    }
                    else BusinessLayerServices.QC_sampleBL().Add(SampleStock);

                    MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                    ReloadSample();
                }
                else MessageBoxHelper.Warning("กรุณากรอกข้อมูลให้ครบถ้วน");

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReloadSample()
        {
            try
            {

                QC_Sample ex = new QC_Sample();
                ex = BusinessLayerServices.QC_sampleBL().GetSingle(Curr_sample);
                if (ex == null) return;

                CuttingDatePicker.SelectedDate = ex.CuttingDate;
                CropTextBox.Text = ex.Crop.ToString();
                TypeCombobox.Text = ex.Type;

                stecGradeCombobox.Text = ex.StecPackedGrade;
                SampleTypeCombobox.Text = ex.SampleType;
                SizeTextBox.Text = ex.CartonDimensions;
                CaseNoTextBox.Text = ex.StecCaseNo;
                QtyTextBox.Text = ex.Quantity.ToString();
                WeightTextBox.Text = ex.Weight.ToString();
                LocationTextBox.Text = ex.Location;
                FormTextBox.Text = ex.TobaccoType;
                NoteTextBox.Text = ex.Note;
                NicTextBox.Text = ex.Nicotine == 0 ? "" : ex.Nicotine.ToString();
                SugTextBox.Text = ex.Sug == 0 ? "" : ex.Sug.ToString();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private bool VerifyBFAdd()
        {
            bool verifyPass = true;
            if (string.IsNullOrEmpty(CropTextBox.Text)) verifyPass = false;
            if (string.IsNullOrEmpty(TypeCombobox.Text)) verifyPass = false;
            if (string.IsNullOrEmpty(stecGradeCombobox.Text))
            {
                MessageBoxHelper.Info("กรุณาเลือก Packed Grade");
                verifyPass = false;
            }
                
            if (string.IsNullOrEmpty(SampleTypeCombobox.Text))
            {
                MessageBoxHelper.Info("กรุณาเลือกประเภทการตัดยา Stock");
                verifyPass = false;
            }
            if (string.IsNullOrEmpty(CuttingDatePicker.Text)) verifyPass = false;
           
            if (string.IsNullOrEmpty(QtyTextBox.Text))
            {
                MessageBoxHelper.Info("กรุณาระบุจำนวนที่ตัดมา");
                QtyTextBox.Focus();
                verifyPass = false;
            }
            if (Helper.RegularExpressionHelper.IsNumericCharacter(QtyTextBox.Text) == false)
            {
                MessageBoxHelper.Info("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");
                QtyTextBox.Focus();
                verifyPass = false;
            }

            if (!string.IsNullOrEmpty(WeightTextBox.Text))
            {
                if (Helper.RegularExpressionHelper.IsDecimalCharacter(WeightTextBox.Text) == false)
                {
                    MessageBoxHelper.Info("Weight จะต้องเป็นตัวเลขเท่านั้น");
                    WeightTextBox.Focus();
                    verifyPass = false;
                }
            }

            if (!string.IsNullOrEmpty(NicTextBox.Text))
            {
                if (Helper.RegularExpressionHelper.IsDecimalCharacter(NicTextBox.Text) == false)
                {
                    MessageBoxHelper.Info("Nicotine จะต้องเป็นตัวเลขเท่านั้น");
                    NicTextBox.Focus();
                    verifyPass = false;
                }
            }
            if (!string.IsNullOrEmpty(SugTextBox.Text))
            {
                if (Helper.RegularExpressionHelper.IsDecimalCharacter(SugTextBox.Text) == false)
                {
                    MessageBoxHelper.Info("Sugar จะต้องเป็นตัวเลขเท่านั้น");
                    SugTextBox.Focus();
                    verifyPass = false;
                }
            }

            return verifyPass;
        }

        private void TypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            //Get Packed Grade
            GetPackedGrade();
        }


        private void GetPackedGrade()
        {
            if (string.IsNullOrEmpty(TypeCombobox.Text) || string.IsNullOrEmpty(CropTextBox.Text)) return;

            var lst = BusinessLayerServices.PackedGradeBL().GetPackedGradeByCrop(Convert.ToInt16(CropTextBox.Text), TypeCombobox.Text);

            packedGradeDataGrid.ItemsSource = null;
            packedGradeDataGrid.ItemsSource = lst;

            TotalTextBlock.Text = lst.Count().ToString();
        }

        private void CropTextBox_KeyUp(object sender, KeyEventArgs e)
        {           
            GetPackedGrade();
        }

        private void packedGradeDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (packedGradeDataGrid.SelectedIndex < 0) return;

                var selected = (packedgrade)packedGradeDataGrid.SelectedItem;
                if (selected != null)
                {
                    stecGradeCombobox.Text = selected.packedgrade1;
                    TypeCombobox.SelectedValue = selected.type;
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
