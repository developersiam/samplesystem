﻿using DomainModel;
using SampleSystem.Helper;
using SampleSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;


namespace SampleSystem.Forms
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Page
    {
        public Login()
        {
            InitializeComponent();
            UsernameTextBox.Focus();
        }
        private void UsernameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length > 0 && PasswordTextBox.Password.Length > 0)
                    LoginButton.IsEnabled = true;
                else
                    LoginButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PasswordTextBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length > 0 && PasswordTextBox.Password.Length > 0)
                    LoginButton.IsEnabled = true;
                else
                    LoginButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            UsernameTextBox.Focus();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UsernameTextBox.Text = "";
                PasswordTextBox.Password = "";
                LoginButton.IsEnabled = false;
                UsernameTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            LoginProcess();
        }
        private void LoginProcess()
        {
            try
            {
                //if (UsernameTextBox.Text.Length <= 0)
                //{
                //    MessageBoxHelper.Warning("โปรดกรอกชื่อผู้ใช้");
                //    return;
                //}
                //if (PasswordTextBox.Password.Length <= 0)
                //{
                //    MessageBoxHelper.Warning("โปรดกรอกรหัสผ่าน");
                //    return;
                //}

                //if (UserAccountHelper.ValidateUserNameInActiveDirectory(UsernameTextBox.Text) == false)
                //    throw new ArgumentException("ไม่พบชื่อผู้ใช้นี้ในระบบ โปรดตรวจสอบกับทางแผนกไอที");

                //if (UserAccountHelper.ActiveDirectoryAuthenticate(UsernameTextBox.Text, PasswordTextBox.Password) == false)
                //    throw new ArgumentException("รหัสผ่านไม่ถูกต้อง");

                //var userAccount = BusinessLayerServices.QC_sample_UserAccountBL().GetSingle(UsernameTextBox.Text);
                //if (userAccount == null)
                //    throw new ArgumentException("บัญชีผู้ใช้งานของท่านยังไม่ได้ลงทะเบียนเข้าใช้งานระบบ โปรดติดต่อแผนกไอทีเพื่อทำการเพิ่มสิทธิ์การใช้งานระบบ");

                //user_setting.User = userAccount;
                //user_setting.UserRoles = BusinessLayerServices.QC_sample_UserRoleBL()
                //    .GetByUsername(userAccount.Username)
                //    .Select(x => new QC_sample_Role
                //    {
                //        RoleID = x.RoleID,
                //        RoleName = x.QC_sample_Role.RoleName
                //    }).ToList();

                this.NavigationService.Navigate(new Home());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }

        }

        private void PasswordTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;

            LoginProcess();
        }
    }
}
