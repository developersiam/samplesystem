﻿using DomainModel;
using SampleSystem.Helper;
using SampleSystem.ViewModel;
using SampleSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SampleSystem.Forms.Customer
{
    /// <summary>
    /// Interaction logic for CustomerWindow.xaml
    /// </summary>
    public partial class CustomerWindow : Window
    {
        int curr_cust;
        int curr_addr;
        public CustomerWindow()
        {
            InitializeComponent();
            BindingAllCustomer();
        }

        private void BindingAllCustomer()
        {
            //List all Customer
            CustomerDataGrid.ItemsSource = null;
            CustomerDataGrid.ItemsSource = Helper.QC_Sample_Customer_Helper.GetAllCustomer();
            //CustomerDataGrid.ItemsSource = BusinessLayerServices.QC_sample_AddrBL().GetAll();
        }

        private void CustomerDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (CustomerDataGrid.SelectedIndex < 0) return;

                var selected = (qc_sample_customer_vm)CustomerDataGrid.SelectedItem;
                if (selected != null)
                {
                    NameTextBox.Text = selected.Name;
                    AttnTextBox.Text = selected.Attn;
                    Addr1TextBox.Text = selected.Addr1;
                    Addr2TextBox.Text = selected.Addr2;
                    Addr3TextBox.Text = selected.Addr3;
                    Addr4TextBox.Text = selected.Addr4;
                    Phone1TextBox.Text = selected.Phone1;
                    Phone2TextBox.Text = selected.Phone2;
                    Email1TextBox.Text = selected.Email1;
                    Email2TextBox.Text = selected.Email2;
                    Notify1TextBox.Text = selected.Notify1;
                    Notify2TextBox.Text = selected.Notify2;
                    Notify3TextBox.Text = selected.Notify3;
                    curr_cust = selected.CustomerId;
                    curr_addr = selected.AddressId;
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(NameTextBox.Text))
                {

                    var cust = new QC_Sample_Customer
                    {
                        Name = NameTextBox.Text,
                        Attn = AttnTextBox.Text,
                    };
                    var addr = new QC_Sample_Address
                    {
                        Addr1 = Addr1TextBox.Text,
                        Addr2 = Addr2TextBox.Text,
                        Addr3 = Addr3TextBox.Text,
                        Addr4 = Addr4TextBox.Text,
                        Phone1 = Phone1TextBox.Text,
                        Phone2 = Phone2TextBox.Text,
                        Email1 = Email1TextBox.Text,
                        Email2 = Email2TextBox.Text,
                        Notify1 = Notify1TextBox.Text,
                        Notify2 = Notify2TextBox.Text,
                        Notify3 = Notify3TextBox.Text,
                    };

                    //Check Duplicated
                    QC_Sample_Customer ex = BusinessLayerServices.QC_sample_CustomerBL().GetSingle(NameTextBox.Text);
                    if (ex != null)
                    {
                        //Binding new one to existed one
                        BusinessLayerServices.QC_sample_CustomerBL().Edit(cust);

                        addr.CustomerId = ex.CustomerId;

                        //Update Addr
                        var a = BusinessLayerServices.QC_sample_AddrBL().GetAllAddrByName(ex.CustomerId, Addr1TextBox.Text);
                        if (a != null) BusinessLayerServices.QC_sample_AddrBL().Edit(addr);
                        else BusinessLayerServices.QC_sample_AddrBL().Add(addr);
                    }
                    else
                    {
                        var custID = BusinessLayerServices.QC_sample_CustomerBL().Add(cust);

                        addr.CustomerId = custID;
                        //Add Addr
                        BusinessLayerServices.QC_sample_AddrBL().Add(addr);
                    };

                    MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                    BindingAllCustomer();
                }
                else MessageBoxHelper.Warning("กรุณากรอกข้อมูลให้ครบถ้วน");

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CustomerDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (qc_sample_customer_vm)CustomerDataGrid.SelectedItem;
                var i = BusinessLayerServices.QC_sample_AddrBL().GetAllAddr(model.CustomerId);

                BusinessLayerServices.QC_sample_AddrBL().DeleteAddr(model.CustomerId, model.AddressId);

                //Delete Customer table if there are one address
                if (i.Count() == 1) BusinessLayerServices.QC_sample_CustomerBL().Delete(model.CustomerId);

                BindingAllCustomer();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }


        }
        private void ClearContent()
        {
            //Clear Textbox
            //cCropTextBox.Text = "";
            //sGradeTextBox.Text = "";
            //cGradeTextBox.Text = "";
            //oGradeTextBox.Text = "";
            //FormTextBox.Text = "";
            //SizeTextBox.Text = "";
            //WeightTextBox.Text = "";
            //sCaseNoTextBox.Text = "";
            //cCaseNoTextBox.Text = "";
            //QtyTextBox.Text = "";
            //unitPriceTextBox.Text = "";
            //NicTextBox.Text = "";
            //SugTextBox.Text = "";
            //SampleInvTextBox.Text = "";
        }
    }
}

