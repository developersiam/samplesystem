﻿using DomainModel;
using SampleSystem.ViewModel;
using SampleSystemBL;
using System.Collections.Generic;
using System.Linq;

namespace SampleSystem.Helper
{
    public static class QC_Sample_Customer_Helper
    {
        public static qc_sample_customer_vm GetCustomerInfoDetails(int custId, int addrId)
        {
            QC_Sample_Address r = new QC_Sample_Address();
            qc_sample_customer_vm info = new qc_sample_customer_vm();
            r = BusinessLayerServices.QC_sample_AddrBL().GetSingle(custId, addrId);
            if (r != null)
            {
                info.Name = r.QC_Sample_Customer.Name;
                info.Attn = r.QC_Sample_Customer.Attn;
                info.Addr1 = r.Addr1;
                info.Addr2 = r.Addr2;
                info.Addr3 = r.Addr3;
                info.Addr4 = r.Addr4;
                info.Email1 = r.Email1;
                info.Email2 = r.Email2;
                info.Phone1 = r.Phone1;
                info.Phone2 = r.Phone2;
                info.Notify1 = r.Notify1;
                info.Notify2 = r.Notify2;
                info.Notify3 = r.Notify3;
                info.Notify4 = r.Notify4;
                info.AddressId = r.AddressId;
                info.CustomerId = r.CustomerId;
            }
            return info;
         }
        public static List<qc_sample_customer_vm> GetCustomerAllAdr(int custId)
        {
            return BusinessLayerServices.QC_sample_AddrBL().GetAllAddr(custId).Select(x => new qc_sample_customer_vm
            {
                Name = x.QC_Sample_Customer.Name,
                Attn = x.QC_Sample_Customer.Attn,
                Addr1 = x.Addr1,
                Addr2 = x.Addr2,
                Addr3 = x.Addr3,
                Addr4 = x.Addr4,
                Email1 = x.Email1,
                Email2 = x.Email2,
                Phone1 = x.Phone1,
                Phone2 = x.Phone2,
                Notify1 = x.Notify1,
                Notify2 = x.Notify2,
                Notify3 = x.Notify3,
                Notify4 = x.Notify4,
                AddressId = x.AddressId,
                CustomerId = x.CustomerId,
            }).ToList();
        }
        public static List<qc_sample_customer_vm> GetAllCustomer()
        {
            return BusinessLayerServices.QC_sample_AddrBL().GetAll().Select(x => new qc_sample_customer_vm
            {
                Name = BusinessLayerServices.QC_sample_CustomerBL().GetById(x.CustomerId).Name,
                Attn = BusinessLayerServices.QC_sample_CustomerBL().GetById(x.CustomerId).Attn,
                Addr1 = x.Addr1,
                Addr2 = x.Addr2,
                Addr3 = x.Addr3,
                Addr4 = x.Addr4,
                Email1 = x.Email1,
                Email2 = x.Email2,
                Phone1 = x.Phone1,
                Phone2 = x.Phone2,
                Notify1 = x.Notify1,
                Notify2 = x.Notify2,
                Notify3 = x.Notify3,
                Notify4 = x.Notify4,
                AddressId = x.AddressId,
                CustomerId = x.CustomerId,
            }).ToList();
        }

    }
}
