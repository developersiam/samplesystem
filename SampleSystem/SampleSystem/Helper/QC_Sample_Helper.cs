﻿using SampleSystem.ViewModel;
using SampleSystemBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleSystem.Helper
{
    public static class QC_Sample_Helper
    {
        public static List<qc_sample_vm> GetSampleByTypeCrop(string t, int c)
        {
            return BusinessLayerServices.QC_sampleBL().GetByCropType(c, t).Select(x => new qc_sample_vm
            {
                CuttingDate = x.CuttingDate,
                SampleType = x.SampleType,
                SampleId = x.SampleId,
                Type = x.Type,
                Crop = x.Crop,
                CustomerCrop = x.CustomerCrop,
                Customer = x.Customer,
                StecPackedGrade = x.StecPackedGrade,
                StecCaseNo = x.StecCaseNo,
                CustomerCaseNo = x.CustomerCaseNo,
                CustomerGrade = x.CustomerGrade,
                OfferGrade = x.OfferGrade,
                CartonDimensions = x.CartonDimensions,
                Quantity = x.Quantity,
                Weight = x.Weight,
                TobaccoType = x.TobaccoType,
                Location = x.Location,
                Note = x.Note,
                Nicotine = x.Nicotine,
                Sug = x.Sug,
                InvoiceNo = BusinessLayerServices.QC_sample_ShippingDBL().GetInvoiceNo(x.SampleId)
            }).ToList();           
           
        }


    }
}
