﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace SampleSystem.Helper
{
    public static class user_setting
    {
        public struct TrueFalseStatus
        {
            public bool Status { get; set; }
            public string StatusName { get; set; }
        }
        public static List<TrueFalseStatus> Status
        {
            get
            {
                List<TrueFalseStatus> list = new List<TrueFalseStatus>();
                list.Add(new TrueFalseStatus { Status = true, StatusName = "Finish" });
                list.Add(new TrueFalseStatus { Status = false, StatusName = "Not Finish" });

                return list;
            }
        }
        public static QC_Sample_UserAccount User { get; set; }
        public static List<QC_sample_Role> UserRoles { get; set; }
    }
}
