﻿using SampleSystem.ViewModel;
using SampleSystemBL;
using System.Collections.Generic;
using System.Linq;

namespace SampleSystem.Helper
{
    public static class QC_Sample_ShippingD_Helper
    {
        public static List<qc_sample_details_vm> GetShippingDetails(string inv)
        {
            return BusinessLayerServices.QC_sample_ShippingDBL().GetShippingList(inv).Select(x => new qc_sample_details_vm
            {
                Shipping_DetailsId = x.Shipping_DetailsId,
                ShippingId = x.ShippingId,
                SampleId = x.SampleId,
                StecPackedGrade = x.StecPackedGrade,
                CustomerGrade = x.CustomerGrade,
                StecCaseNo = x.StecCaseNo,
                CustomerCaseNo = x.CustomerCaseNo,
                Form = x.Form,
                Weight = x.Weight,
                Quantity = x.Quantity,
                UnitPrices = x.UnitPrices,
                TotalPrices = x.UnitPrices != null && x.Weight != null ? (decimal)x.UnitPrices * (decimal)x.Weight : 0,
                Type = BusinessLayerServices.QC_sampleBL().GetSingle(x.SampleId).Type,
            }).ToList();
        } 
    }
}
