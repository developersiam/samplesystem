//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class QC_Sample
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public QC_Sample()
        {
            this.QC_Sample_Shipping_Details = new HashSet<QC_Sample_Shipping_Details>();
        }
    
        public int SampleId { get; set; }
        public int Crop { get; set; }
        public Nullable<int> CustomerCrop { get; set; }
        public string Customer { get; set; }
        public string Type { get; set; }
        public string SampleType { get; set; }
        public string TobaccoType { get; set; }
        public string StecPackedGrade { get; set; }
        public string CustomerGrade { get; set; }
        public string OfferGrade { get; set; }
        public string StecCaseNo { get; set; }
        public string CustomerCaseNo { get; set; }
        public string CartonDimensions { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> Weight { get; set; }
        public string Location { get; set; }
        public Nullable<decimal> Nicotine { get; set; }
        public Nullable<decimal> Sug { get; set; }
        public System.DateTime CuttingDate { get; set; }
        public string Note { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> Status { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QC_Sample_Shipping_Details> QC_Sample_Shipping_Details { get; set; }
    }
}
