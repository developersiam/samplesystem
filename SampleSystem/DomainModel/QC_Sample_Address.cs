//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class QC_Sample_Address
    {
        public int AddressId { get; set; }
        public int CustomerId { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string Addr3 { get; set; }
        public string Addr4 { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Notify1 { get; set; }
        public string Notify2 { get; set; }
        public string Notify3 { get; set; }
        public string Notify4 { get; set; }
    
        public virtual QC_Sample_Customer QC_Sample_Customer { get; set; }
    }
}
