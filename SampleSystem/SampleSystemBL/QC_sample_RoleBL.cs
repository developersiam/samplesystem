﻿using DomainModel;
using SampleSystemDAL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SampleSystemBL
{
    public interface IQC_sample_RoleBL
    {
        void Add(QC_sample_Role model);
        void Edit(QC_sample_Role model);
        void Delete(Guid id);
        QC_sample_Role GetSingle(Guid id);
        List<QC_sample_Role> GetAll();
    }
    public class QC_sample_RoleBL : IQC_sample_RoleBL
    {
        public void Add(QC_sample_Role model)
        {
            try
            {
                if (DataAccessLayerServices.QC_Sample_RoleRepository()
                    .GetList(x => x.RoleName == model.RoleName)
                    .Count() > 0)
                    throw new ArgumentException("role " + model.RoleName + " นี้มีซ้ำแล้วในระบบ");

                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                model.ModifiedBy = model.CreateBy;

                DataAccessLayerServices.QC_Sample_RoleRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(QC_sample_Role model)
        {
            try
            {
                var editModel = GetSingle(model.RoleID);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.RoleName = model.RoleName;
                editModel.ModifiedBy = model.ModifiedBy;
                editModel.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.QC_Sample_RoleRepository().Update(editModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(Guid id)
        {
            try
            {
                var model = GetSingle(id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.QC_Sample_UserRole.Count() > 0)
                    throw new ArgumentException("Role นี้ถูกกำหนดให้ user บางรายไปแล้ว โปรดลบข้อมูลการกำหนดสิทธิ์ออก่กอน");

                DataAccessLayerServices.QC_Sample_RoleRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public QC_sample_Role GetSingle(Guid roleID)
        {
            return DataAccessLayerServices.QC_Sample_RoleRepository().GetSingle(x => x.RoleID == roleID);
        }
        public List<QC_sample_Role> GetAll()
        {
            return DataAccessLayerServices.QC_Sample_RoleRepository().GetAll().ToList();
        }
    }
}