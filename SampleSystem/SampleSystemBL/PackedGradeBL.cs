﻿using DomainModel;
using SampleSystemDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleSystemBL
{
    public interface IPackedGradeBL
    {
        List<packedgrade> GetPackedGradeByCrop(int crop, string type);
    }
    public class PackedGradeBL : IPackedGradeBL
    {
        public List<packedgrade> GetPackedGradeByCrop(int crop, string type)
        {
            List<packedgrade> packlst = new List<packedgrade>();
            packlst = DataAccessLayerServices.PackedGradeRepository().GetList(s => s.crop == crop && s.type == type).OrderBy(c => c.packedgrade1).ToList();

            return packlst;
        }
    }
}
