﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleSystemBL
{
    public static class BusinessLayerServices
    {
        public static IQC_sample_RoleBL QC_sample_RoleBL()
        {
            IQC_sample_RoleBL obj = new QC_sample_RoleBL();
            return obj;
        }
        public static IQC_sample_UserAccountBL QC_sample_UserAccountBL()
        {
            IQC_sample_UserAccountBL obj = new QC_sample_UserAccountBL();
            return obj;
        }
        public static IQC_sample_UserRoleBL QC_sample_UserRoleBL()
        {
            IQC_sample_UserRoleBL obj = new QC_sample_UserRoleBL();
            return obj;
        }
        public static ITypeBL TypeBL()
        {
            ITypeBL obj = new TypeBL();
            return obj;
        }
        public static IPackedGradeBL PackedGradeBL()
        {
            IPackedGradeBL obj = new PackedGradeBL();
            return obj;
        }
        public static IQC_sampleBL QC_sampleBL()
        {
            IQC_sampleBL obj = new QC_sampleBL();
            return obj;
        }
        public static IQC_sample_ShippingBL QC_sample_ShippingBL()
        {
            IQC_sample_ShippingBL obj = new QC_sample_ShippingBL();
            return obj;
        }
        public static IQC_sample_ShippingDBL QC_sample_ShippingDBL()
        {
            IQC_sample_ShippingDBL obj = new QC_sample_ShippingDBL();
            return obj;
        }
        public static IQC_sample_CustomerBL QC_sample_CustomerBL()
        {
            IQC_sample_CustomerBL obj = new QC_sample_CustomerBL();
            return obj;
        }
        public static IQC_sample_AddressBL QC_sample_AddrBL()
        {
            IQC_sample_AddressBL obj = new QC_sample_AddressBL();
            return obj;
        }
    }
}
