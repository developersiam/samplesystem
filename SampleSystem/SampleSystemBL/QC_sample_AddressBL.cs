﻿using DomainModel;
using SampleSystemDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleSystemBL
{
    public interface IQC_sample_AddressBL
    {
        void Add(QC_Sample_Address model);
        void Edit(QC_Sample_Address model);
        void DeleteAddr(int id, int addrID);
        //void DeletebyInvno(int Shippingid);
        QC_Sample_Address GetSingle(int custID, int addrID);
        List<QC_Sample_Address> GetAllAddr(int custID);
        QC_Sample_Address GetAllAddrByName(int custID, string Addr1);
        List<QC_Sample_Address> GetAll();

    }
    public class QC_sample_AddressBL : IQC_sample_AddressBL
    {
        public void Add(QC_Sample_Address model)
        {
            try
            {
                if (GetSingleByAddr(model.CustomerId, model.Addr1) != null)
                    throw new ArgumentException("ข้อมุล Customer Address นี้มีแล้วในระบบ");


                DataAccessLayerServices.QC_Sample_AddrRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(QC_Sample_Address model)
        {
            try
            {
                //var editModel = GetSingle(model.SampleId);
                var editModel = GetSingleByAddr(model.CustomerId, model.Addr1);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");
                editModel.CustomerId = model.CustomerId;
                //editModel.AddressId = model.AddressId;
                editModel.Addr1 = model.Addr1;
                editModel.Addr2 = model.Addr2;
                editModel.Addr3 = model.Addr3;
                editModel.Addr4 = model.Addr4;
                editModel.Phone1 = model.Phone1;
                editModel.Phone2 = model.Phone2;
                editModel.Email1 = model.Email1;
                editModel.Email2 = model.Email2;
                editModel.Notify1 = model.Notify1;
                editModel.Notify2 = model.Notify2;
                editModel.Notify3 = model.Notify3;
                editModel.Notify4 = model.Notify3;

                DataAccessLayerServices.QC_Sample_AddrRepository().Update(editModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public QC_Sample_Address GetSingle(int cID, int aID)
        {
            return DataAccessLayerServices.QC_Sample_AddrRepository().GetSingle(x => x.CustomerId == cID && x.AddressId == aID);
        }
        public List<QC_Sample_Address> GetAllAddr(int cID)
        {
            return DataAccessLayerServices.QC_Sample_AddrRepository().GetList(x => x.CustomerId == cID).ToList();
        }
        public QC_Sample_Address GetAllAddrByName(int custID, string Addr1)
        {
            return DataAccessLayerServices.QC_Sample_AddrRepository().GetSingle(x => x.CustomerId == custID && x.Addr1 == Addr1);
        }
        public List<QC_Sample_Address> GetAll()
        {
            return DataAccessLayerServices.QC_Sample_AddrRepository().GetAll().ToList();
        }
        public QC_Sample_Address GetSingleByAddr(int cID, string addr1)
        {
            return DataAccessLayerServices.QC_Sample_AddrRepository().GetSingle(x => x.CustomerId == cID && x.Addr1 == addr1);
        }
        public void DeleteAddr(int id, int addrID)
        {
            try
            {
                var model = GetSingle(id,addrID);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                DataAccessLayerServices.QC_Sample_AddrRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

