﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using SampleSystemDAL;

namespace SampleSystemBL
{
    public interface IQC_sample_UserAccountBL
    {
        void Add(QC_Sample_UserAccount model);
        void Delete(string username);
        QC_Sample_UserAccount GetSingle(string username);
        List<QC_Sample_UserAccount> GetAll();
    }
    public class QC_sample_UserAccountBL : IQC_sample_UserAccountBL
    {
        public void Add(QC_Sample_UserAccount model)
        {
            try
            {
                if (DataAccessLayerServices.QC_Sample_UserAccountRepository()
                    .GetList(x => x.Username == model.Username)
                    .Count() > 0)
                    throw new ArgumentException("username " + model.Username + " นี้มีซ้ำแล้วในระบบ");

                model.CreateDate = DateTime.Now;
                DataAccessLayerServices.QC_Sample_UserAccountRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string username)
        {
            try
            {
                var model = GetSingle(username);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.QC_Sample_UserRole.Count() > 0)
                    throw new ArgumentException("ไม่สามารถลบผู้ใช้นี้ได้เนื่องจากยังมีข้อมูลการกำหนดสิทธิ์การใช้งานระบบอยู่ โปรดลบสิทธิ์การใช้งานระบบออกก่อน");

                DataAccessLayerServices.QC_Sample_UserAccountRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<QC_Sample_UserAccount> GetAll()
        {
            return DataAccessLayerServices.QC_Sample_UserAccountRepository().GetAll().ToList();
        }

        public QC_Sample_UserAccount GetSingle(string username)
        {
            return DataAccessLayerServices.QC_Sample_UserAccountRepository()
                .GetSingle(x => x.Username == username);
        }
    }
}
