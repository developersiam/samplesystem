﻿using DomainModel;
using SampleSystemDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleSystemBL
{
    public interface IQC_sample_UserRoleBL
    {
        void Add(QC_Sample_UserRole model);
        void Delete(string username, Guid roleID);
        QC_Sample_UserRole GetSingle(string username, Guid roleID);
        List<QC_Sample_UserRole> GetAll();
        List<QC_Sample_UserRole> GetByUsername(string username);
    }
    public class QC_sample_UserRoleBL : IQC_sample_UserRoleBL
    {
        public void Add(QC_Sample_UserRole model)
        {
            try
            {
                var list = GetByUsername(model.Username);
                if (list.Where(x => x.RoleID == model.RoleID).Count() > 0)
                    throw new ArgumentException("มีการกำหนด role นี้ซ้ำแล้วในระบบ");

                model.CreateDate = DateTime.Now;
                DataAccessLayerServices.QC_Sample_UserRoleRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string username, Guid roleID)
        {
            try
            {
                var model = GetSingle(username, roleID);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                DataAccessLayerServices.QC_Sample_UserRoleRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<QC_Sample_UserRole> GetByUsername(string username)
        {
            return DataAccessLayerServices.QC_Sample_UserRoleRepository()
                .GetList(x => x.Username == username,
                x => x.QC_sample_Role).ToList();
        }

        public QC_Sample_UserRole GetSingle(string username, Guid roleID)
        {
            return DataAccessLayerServices.QC_Sample_UserRoleRepository()
                .GetSingle(x => x.Username == username && x.RoleID == roleID);
        }
        public List<QC_Sample_UserRole> GetAll()
        {
            return DataAccessLayerServices.QC_Sample_UserRoleRepository()
                .GetAll(x => x.QC_sample_Role)
                .ToList();
        }
    }
}
