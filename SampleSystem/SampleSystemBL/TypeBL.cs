﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleSystemDAL;

namespace SampleSystemBL
{
    public interface ITypeBL
    {
        List<string> GetAllTypes();
    }
    public class TypeBL : ITypeBL
    {
        public List<string> GetAllTypes()
        {
            var allt = new List<string>();
            allt = DataAccessLayerServices.TypeRepository().GetAll().OrderBy(c => c.type1).Select(w => w.type1).Distinct().ToList();
            return allt;
        }
    }
}
