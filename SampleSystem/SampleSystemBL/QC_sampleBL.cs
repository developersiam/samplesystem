﻿using DomainModel;
using SampleSystemDAL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SampleSystemBL
{
    public struct SampleCuttingCrop
    {
        public int crop { get; set; }
    }
    public interface IQC_sampleBL
    {
        void Add(QC_Sample model);
        void Edit(QC_Sample model);
        void Delete(int id);
        QC_Sample ExistedSample(int crop, string type, string grade, DateTime cuttingDate, string stecCaseno);
        QC_Sample GetSingle(int id);
        List<QC_Sample> GetAll();
        List<QC_Sample> GetByCrop(int crop);
        List<QC_Sample> GetByCropType(int crop, string t);
        List<int> GetCuttingCrop();
        List<int> GetCrop();
    }
    public class QC_sampleBL : IQC_sampleBL
    {
        public void Add(QC_Sample model)
        {
            try
            {
                if (DataAccessLayerServices.QC_SampleRepository()
                    .GetList(x => x.Crop == model.Crop && x.Type == model.Type && x.StecPackedGrade == model.StecPackedGrade && x.CuttingDate == model.CuttingDate && x.StecCaseNo == model.StecCaseNo)
                    .Count() > 0)
                    throw new ArgumentException("ข้อมุล sample นี้มีซ้ำแล้วในระบบ");

                model.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.QC_SampleRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(QC_Sample model)
        {
            try
            {
                //var editModel = GetSingle(model.SampleId);
                var editModel = ExistedSample(model.Crop, model.Type, model.StecPackedGrade, (DateTime)model.CuttingDate, model.StecCaseNo);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.Crop = model.Crop;
                editModel.Type = model.Type;
                editModel.SampleType = model.SampleType;
                editModel.StecPackedGrade = model.StecPackedGrade;
                editModel.StecCaseNo = model.StecCaseNo;
                editModel.CartonDimensions = model.CartonDimensions;
                editModel.Quantity = model.Quantity;
                editModel.Weight = model.Weight;
                editModel.Location = model.Location;
                editModel.CuttingDate = model.CuttingDate;
                editModel.Note = model.Note;
                editModel.TobaccoType = model.TobaccoType;
                editModel.Nicotine = model.Nicotine;
                editModel.Sug = model.Sug;
                editModel.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.QC_SampleRepository().Update(editModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public QC_Sample GetSingle(int sampleId)
        {
            return DataAccessLayerServices.QC_SampleRepository().GetSingle(x => x.SampleId == sampleId);
        }
        public void Delete(int id)
        {
            try
            {
                var model = GetSingle(id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                DataAccessLayerServices.QC_SampleRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<QC_Sample> GetAll()
        {
            return DataAccessLayerServices.QC_SampleRepository().GetAll().ToList();
        }
        public List<QC_Sample> GetByCrop(int c)
        {
            List<QC_Sample> lst = DataAccessLayerServices.QC_SampleRepository().GetList(x => x.CuttingDate.Year == c).ToList();

            return lst;
        }
        public List<QC_Sample> GetByCropType(int crop, string t)
        {
            //List<QC_Sample> lst = DataAccessLayerServices.QC_SampleRepository().GetList(x => x.CuttingDate.Year == crop && x.Type == t).ToList();

            List<QC_Sample> lst = DataAccessLayerServices.QC_SampleRepository().GetList(x => x.Crop == crop && x.Type == t).ToList();

            return lst;
        }
        public QC_Sample ExistedSample(int crop, string type, string grade, DateTime cuttingDate, string Caseno)
        {
            QC_Sample Sid = new QC_Sample();
            Sid = DataAccessLayerServices.QC_SampleRepository().GetSingle(x => x.Crop == crop && x.Type == type && x.StecPackedGrade == grade && x.CuttingDate == cuttingDate && x.StecCaseNo == Caseno);
            return Sid;
        }
        public List<int> GetCuttingCrop()
        {
            try
            {
                List<int> cCrop = new List<int>();
                var cCropLst = DataAccessLayerServices.QC_SampleRepository().GetAll().ToList();
                var s = cCropLst.Select(x => x.CuttingDate.Year).ToList();
                foreach (var i in s)
                {
                    if (!cCrop.Contains(i)) cCrop.Add(i);
                }

                return cCrop;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<int> GetCrop()
        {
            try
            {
                var cCropLst = DataAccessLayerServices.QC_SampleRepository().GetAll().Select(x => x.Crop).Distinct().ToList();
                return cCropLst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
