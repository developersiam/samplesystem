﻿using DomainModel;
using SampleSystemDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleSystemBL
{
    public interface IQC_sample_ShippingDBL
    {
        void Add(QC_Sample_Shipping_Details model);
        void Edit(QC_Sample_Shipping_Details model);
        void DeleteSampleItem(int id);
        void DeletebyInvno(int Shippingid);
        QC_Sample_Shipping_Details GetSingle(int shippingId, int sampleId);
        List<QC_Sample_Shipping_Details> GetShippingList(string invno);
        string GetInvoiceNo(int sampleId);

    }
    public class QC_sample_ShippingDBL : IQC_sample_ShippingDBL
    {
        public void Add(QC_Sample_Shipping_Details model)
        {
            try
            {
                if (DataAccessLayerServices.QC_Sample_ShipDetailsRepository()
                    .GetList(x => x.Crop == model.Crop && x.SampleId == model.SampleId)
                    .Count() > 0)
                    throw new ArgumentException("ข้อมุล sample นี้ถูกนำไปใช้ใน Invoice อื่น ๆ แล้ว");

                model.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.QC_Sample_ShipDetailsRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(QC_Sample_Shipping_Details model)
        {
            try
            {
                var editModel = GetSingle(model.ShippingId, model.SampleId);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.Shipping_DetailsId = model.Shipping_DetailsId;
                editModel.Crop = model.Crop;
                editModel.CustomerCrop = model.CustomerCrop;
                editModel.SampleId = model.SampleId;
                editModel.SampleType = model.SampleType;
                editModel.StecPackedGrade = model.StecPackedGrade;
                editModel.CustomerGrade = model.CustomerGrade;
                editModel.OfferGrade = model.OfferGrade;
                editModel.Form = model.Form;
                editModel.StecCaseNo = model.StecCaseNo;
                editModel.CustomerCaseNo = model.CustomerCaseNo;
                editModel.Quantity = model.Quantity;
                editModel.UnitPrices = model.UnitPrices;
                editModel.Location = model.Location;
                editModel.User = model.User;
                editModel.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.QC_Sample_ShipDetailsRepository().Update(editModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public QC_Sample_Shipping_Details GetSingle(int shippingId, int sampleId)
        {
            return DataAccessLayerServices.QC_Sample_ShipDetailsRepository().GetSingle(x => x.ShippingId == shippingId && x.SampleId == sampleId);
        }
        public List<QC_Sample_Shipping_Details> GetShippingList(string invno)
        {
            return DataAccessLayerServices.QC_Sample_ShipDetailsRepository().GetList(x => x.QC_Sample_Shipping.InvoiceNo == invno).ToList();
        }
        public string GetInvoiceNo(int sampleId)
        {
            string Invno = "";
            var i = DataAccessLayerServices.QC_Sample_ShipDetailsRepository().GetSingle(x => x.SampleId == sampleId);
            if (i != null) Invno = i.QC_Sample_Shipping.InvoiceNo; //else Invno = "";

            return Invno;                        
        }
        public void DeleteSampleItem(int id)
        {
            try
            {
                var model = DataAccessLayerServices.QC_Sample_ShipDetailsRepository().GetSingle(x => x.Shipping_DetailsId == id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                DataAccessLayerServices.QC_Sample_ShipDetailsRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeletebyInvno(int Shippingid)
        {
            try
            {
                var modelLst = DataAccessLayerServices.QC_Sample_ShipDetailsRepository().GetList(x => x.ShippingId == Shippingid);
                if (modelLst == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                foreach(var i in modelLst)
                {
                    DataAccessLayerServices.QC_Sample_ShipDetailsRepository().Remove(i);
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
