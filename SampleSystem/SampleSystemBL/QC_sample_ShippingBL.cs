﻿using DomainModel;
using SampleSystemDAL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SampleSystemBL
{
    public interface IQC_sample_ShippingBL
    {
        int Add(QC_Sample_Shipping model);
        void Edit(QC_Sample_Shipping model);
        void Delete(string Invoice);
        QC_Sample_Shipping GetSingle(string inv);
        List<int> GetInvoiceCrop();
        List<QC_Sample_Shipping> GetShippingDataByCrop(int crop);
    }
    public  class QC_sample_ShippingBL : IQC_sample_ShippingBL
    {
        int shippingId;
        public int Add(QC_Sample_Shipping model)
        {
            try
            {
                if (GetSingle(model.InvoiceNo) != null)
                    throw new ArgumentException("ข้อมุล Invoice นี้มีแล้วในระบบ");

                model.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.QC_Sample_ShippingRepository().Add(model);

                var newShipping = GetSingle(model.InvoiceNo);
                if (newShipping != null)  shippingId = newShipping.ShippingId;

                return shippingId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(QC_Sample_Shipping model)
        {
            try
            {                
                var editModel = GetSingle(model.InvoiceNo);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.Crop = model.Crop;
                editModel.InvoiceNo = model.InvoiceNo;
                editModel.Customer = model.Customer;
                editModel.CustomerId = model.CustomerId;
                editModel.AddressId = model.AddressId;
                editModel.Gross_wt = model.Gross_wt;
                editModel.Tare_wt = model.Tare_wt;
                editModel.Total_Qty = model.Total_Qty;
                editModel.Destination = model.Destination;
                editModel.Attn = model.Attn;
                editModel.IncoTerm = model.IncoTerm;
                editModel.ABW = model.ABW;
                editModel.IncoTerm = model.IncoTerm;
                editModel.Courier = model.Courier;
                editModel.Port = model.Port;
                editModel.Note = model.Note;
                editModel.DateTo = model.DateTo;
                editModel.User = model.User;
                editModel.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.QC_Sample_ShippingRepository().Update(editModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public QC_Sample_Shipping GetSingle(string inv)
        {
            QC_Sample_Shipping Sid = new QC_Sample_Shipping();
            Sid = DataAccessLayerServices.QC_Sample_ShippingRepository().GetSingle(x => x.InvoiceNo == inv);
            return Sid;
        }
        public List<int> GetInvoiceCrop()
        {
            List<int> invCrop = new List<int>();
            var c = DataAccessLayerServices.QC_Sample_ShippingRepository().GetAll().OrderByDescending(x => x.Crop).Select(x => x.Crop).ToList();

            //Get List
            foreach(var i in c)
            {
                if (!invCrop.Contains(i)) invCrop.Add(i);
            }

            return invCrop;
        }
        public List<QC_Sample_Shipping> GetShippingDataByCrop(int crop)
        {
            return DataAccessLayerServices.QC_Sample_ShippingRepository().GetList(x => x.Crop == crop).ToList();
        }
        public void Delete(string inv)
        {
            try
            {
                var model = GetSingle(inv);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                DataAccessLayerServices.QC_Sample_ShippingRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
