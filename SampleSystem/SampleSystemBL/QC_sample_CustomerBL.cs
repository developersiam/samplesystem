﻿using DomainModel;
using SampleSystemDAL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SampleSystemBL
{
    public interface IQC_sample_CustomerBL
    {
        List<QC_Sample_Customer> GetAll();
        int Add(QC_Sample_Customer model);
        void Edit(QC_Sample_Customer model);
        void Delete(int id);
        QC_Sample_Customer GetSingle(string name);
        QC_Sample_Customer GetById(int custID);
    }
    public class QC_sample_CustomerBL : IQC_sample_CustomerBL
    {
        int customerId;
        public List<QC_Sample_Customer> GetAll()
        {
            return DataAccessLayerServices.QC_Sample_CustomerRepository().GetAll().ToList();
        }

        public int Add(QC_Sample_Customer model)
        {
            try
            {
                if (GetSingle(model.Name) != null)
                    throw new ArgumentException("ข้อมุล Customer นี้มีแล้วในระบบ");


                DataAccessLayerServices.QC_Sample_CustomerRepository().Add(model);

                var newCust = GetSingle(model.Name);
                if (newCust != null) customerId = newCust.CustomerId;

                return customerId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(QC_Sample_Customer model)
        {
            try
            {
                //var editModel = GetSingle(model.SampleId);
                var editModel = GetSingle(model.Name);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");
                editModel.Name = model.Name;
                editModel.Attn = model.Attn;
                //editModel.Addr1 = model.Addr1;
                //editModel.Addr2 = model.Addr2;
                //editModel.Addr3 = model.Addr3;
                //editModel.Addr4 = model.Addr4;
                //editModel.Addr5 = model.Addr5;
                //editModel.Addr6 = model.Addr6;
                //editModel.Phone1 = model.Phone1;
                //editModel.Phone2 = model.Phone2;
                //editModel.Email1 = model.Email1;
                //editModel.Email2 = model.Email2;
                //editModel.Notify1 = model.Notify1;
                //editModel.Notify2 = model.Notify2;
                //editModel.Notify3 = model.Notify3;

                DataAccessLayerServices.QC_Sample_CustomerRepository().Update(editModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(int id)
        {
            try
            {
                var model = DataAccessLayerServices.QC_Sample_CustomerRepository().GetSingle(x => x.CustomerId == id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                DataAccessLayerServices.QC_Sample_CustomerRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public QC_Sample_Customer GetSingle(string name)
        {
            QC_Sample_Customer Cus = new QC_Sample_Customer();
            Cus = DataAccessLayerServices.QC_Sample_CustomerRepository().GetSingle(x => x.Name == name);
            return Cus;
        }
        public QC_Sample_Customer GetById(int cID)
        {
            QC_Sample_Customer Cus = new QC_Sample_Customer();
            Cus = DataAccessLayerServices.QC_Sample_CustomerRepository().GetSingle(x => x.CustomerId == cID);
            return Cus;
        }
    }
}
